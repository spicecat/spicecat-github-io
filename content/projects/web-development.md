---
title: Web Development
type: page
---

## [Royale Alchemist Web](https://spicecat.github.io/royale-alchemist/)
wip
[Source](https://github.com/spicecat/qOverflow)

## [qOverflow](https://spicecat.github.io/qoverflow/)
An online forum where members can ask and answer each other's questions, vote on answers, and self-moderate.  
Submission for [BDPA's 2022 National High School Coding Competition](https://github.com/nhscc/problem-statements/tree/main/2022/qoverflow)  
[Source](https://github.com/spicecat/qOverflow)

## [Ghostmeme](https://spicecat.github.io/ghostmeme/)
A messaging app where users can send and receive public and private picture messages.  
Submission for [BDPA's 2021 National High School Coding Competition](https://github.com/nhscc/problem-statements/tree/main/2021/ghostmeme)  
[Frontend Source](https://github.com/spicecat/ghostmeme)  
[Backend Source](https://github.com/spicecat/ghostmemebackend)

## [BDPA Airports](https://spicecat.github.io/airports/)
A website for viewing and booking flights to and from a simuated local airport.  
Submission for [BDPA's 2020 National High School Coding Competition](https://github.com/nhscc/problem-statements/tree/main/2020/airports)  
[Frontend Source](https://github.com/spicecat/ghostmeme)  
[Backend Source](https://github.com/spicecat/ghostmemebackend)

## [OpenContest](https://github.com/LadueCS/OpenContest)
OpenContest is a simple, open, and federated HTTPS JSON protocol to enable anyone to develop and host online programming contests!