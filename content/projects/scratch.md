---
title: Scratch
type: "page"
---
# [PusheenGames23](https://scratch.mit.edu/users/PusheenGames23/)
Main Scratch account

## [What the Hex?](https://scratch.mit.edu/projects/255311389/)
![What the Hex?](https://uploads.scratch.mit.edu/get_image/project/255311389_480x360.png)
A color guessing game to learn hex color codes. Inspired [Hex Codes & Color Theory
](https://www.youtube.com/watch?v=xlRiLSDdqcY) and based on the original [What the Hex?](http://yizzle.com/whatthehex/).

## [Handulum Physics](https://scratch.mit.edu/projects/311056907/)
![Handulum Physics](https://uploads.scratch.mit.edu/get_image/project/311056907_480x360.png)
A physics engine for the Handulum game. Based on [Handulum](http://uploads.ungrounded.net/alternate/1263000/1263490_alternate_71105_r9.zip/) by Stuffed Wombat.

## [Bezier Curve with Multiple Points](https://scratch.mit.edu/projects/206380452/)
![Bezier Curve with Multiple Points](https://uploads.scratch.mit.edu/get_image/project/206380452_480x360.png)
A visual demostration for constructing [Bézier curves](https://javascript.info/bezier-curve).

## [How to lag your computer, Pusheen style.](https://scratch.mit.edu/projects/41853952/)
![How to lag your computer, Pusheen style.](https://uploads.scratch.mit.edu/get_image/project/41853952_480x360.png)
Learn to meow in 85 languages!

## [Compressed Cloud Encoder](https://scratch.mit.edu/projects/238658861/)
![Compressed Cloud Encoder](https://uploads.scratch.mit.edu/get_image/project/238658861_480x360.png)
Compress text data using a modified Huffman Encoding algorithm. Useful for size-limited cloud variables. 

## [Rotating Pen Text Engine](https://scratch.mit.edu/projects/204166214/)
![Rotating Pen Text Engine](https://uploads.scratch.mit.edu/get_image/project/204166214_480x360.png)
A rotating pen text engine!

## [Custom Mouse Cursor](https://scratch.mit.edu/projects/315369204/)
![Custom Mouse Cursor](https://uploads.scratch.mit.edu/get_image/project/315369204_480x360.png)
Custom mouse cursors! Uses [Accurate Mouse XY Limit Workaround](https://scratch.mit.edu/projects/314585546/) to work offscreen. 