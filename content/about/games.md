---
title: "Games"
type: page
---

# Active

## Clash Royale
F2P; Highest trophies: 7385; Highest rank: 2054; Drill main, lava/log bait secondary
[Clashingcat](https://royaleapi.com/player/920C228C) [f8ecf4](https://royaleapi.com/player/JPLV9VRVL)

## Minecraft
daily arcade/duels quest grind; Skywars/CTW main
[WhereisMyCat](https://plancke.io/hypixel/player/stats/WhereisMyCat)

## Genshin Impact
Rerolling 36 star 100k+ primo f2p god

# Inactive

## The Battle Cats
EN JP seed tracker abuser

## Chess
highest rating (USCF): 1917
[lichess](https://lichess.org/@/zippycollar)  
[chess.com](https://www.chess.com/member/zippycollar)  
[USCF](https://www.uschess.org/msa/MbrDtlMain.php?15201162)

## Polytopia
kickoo theme best
