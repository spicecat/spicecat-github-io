---
title: "Languages I Know"
type: page
---


Programming languages that I know:

- **A lot**: Python, JavaScript, Java, HTML, C++

- **Some**: CSS, LaTeX, Spanish

Check out [my projects](/projects) for major programming projects.
